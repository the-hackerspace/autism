const convertMapToRecord = <K extends string, V extends unknown>(m: Map<K, V>): Record<K, V> => [...m].reduce(
    (a, { 0: k, 1: v }) => ({...a, [k]: v}),
    {} as Record<K, V>
);

const record = convertMapToRecord(new Map<'foo', 'bar'>([['foo', 'bar']]));
console.log('foo' in record ? record.foo : 'autist!');
